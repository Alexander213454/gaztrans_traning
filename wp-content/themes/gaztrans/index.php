<?php get_header(); $page_content = get_fields(); ?>


<main>

    <div class="first_block">
        <div class="background" style="background: url('<?php echo($page_content['slider'][0]['background']['url']);?>');">
            <div class="position">
                <h1><?php echo($page_content['slider'][0]['heading']);?></h1>
                <p><?php echo($page_content['slider'][0]['paragraph']);?></p>
                <a href="">Каталог</a>
            </div>    
        </div>
        <a href="next"></a>
        <a href="previous"></a>
    </div>

</main>


<?php get_footer(); ?>