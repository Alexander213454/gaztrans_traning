const webpack = require('webpack');
const path = require('path');
const miniCss = require('mini-css-extract-plugin');
const webpackStream = require('webpack-stream');

module.exports = (env) => {
  const mode = env.mode === 'production' ? 'production' : 'development';
  const watch = env.watch === 'watch';

  return {
    mode: mode,
    devtool: 'source-map',
    entry: './src/js',
    output: {
      path: path.resolve(__dirname, 'js'),
      filename: '../dist/index.js'
    },
    // stats: 'detailed',
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: miniCss.loader
            },
            {
              loader: 'css-loader',
              options: {
                url: false,
              }
            },
            {
              loader: 'sass-loader'
            },
          ]
        },
        {
          test: /fancybox[\/\\]dist[\/\\]js[\/\\]jquery.fancybox.cjs.js/,
          use: "imports-loader?jQuery=jquery,$=jquery,this=>window"
        }
      ]
    },
    plugins: [
      new miniCss({
        filename: `../dist/style.css`,
      }),
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jquery": "jquery"
      }),
    ],
    watch: (!mode || watch),
    watchOptions: {
      ignored: [
        'node_modules/**',
      ]
    },
    externals: {
      jquery: 'jQuery',
    },
  }
}